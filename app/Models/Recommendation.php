<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Recommendation extends Model
{
    use HasFactory;


    /**
     * Get the user associated with the recommandation.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }


    /**
     * Get the movie associated with the recommandation.
     */
    public function movie()
    {
        return $this->belongsTo(Movie::class);
    }


}
