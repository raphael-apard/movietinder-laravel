<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Recommendation;

class AdminRecommendationController extends Controller
{
    
  // Liste toutes les recommandations
	public function list() {

		$recos = Recommendation::all();
		return view('admin.recommendations', ['recos' => $recos]);
	}

}
