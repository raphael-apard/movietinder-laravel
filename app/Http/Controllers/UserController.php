<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

    //Function to show all users
    public function index()
    {
        $users = User::all();
        return view('admin.index', ['users' => $users]);
    }


    //Function to input edits to user
    public function edit($id)
    {
        $user = user::find($id);
        return view('admin.edit', compact('user'));
    }


    //Function to push edit to database
    public function update(Request $request, $id)
    {
        $user = user::find($id);
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->role = $request->input('role');
        $user->update();
        return redirect()->back()->with('status','User has been successfully updated');
    }


    //Function to delete user
    public function delete($id)
    {
        $user = user::find($id);
        $user->delete();
        return redirect()->back()->with('status','User has been successfully deleted');
    }

    //Function to block/deblock user
    public function changeStatus($user, $status)
    {
        $userToUpdate = User::find($user);
        if ($userToUpdate){
            $userToUpdate->status = $status;
            $userToUpdate->save();
        }
        return redirect()->back()->with('status','User has been successfully deleted');
    }

    //Function to search users
    public function search(Request $request)
    {
        $search = $request->input('search');
        $users = User::query()
            ->where('name', 'LIKE', "%{$search}%")
            ->orWhere('email', 'LIKE', "%{$search}%")
            ->get();
        return view('admin.index', compact('users'));
    }
}