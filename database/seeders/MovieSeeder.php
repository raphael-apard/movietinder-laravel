<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class MovieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      for($i = 0; $i < 20; $i++) {
        DB::table('movies')->insert([
            'title' => Str::random(10),
            'synopsis' => Str::random(30),
        ]);
      }
    }
}
