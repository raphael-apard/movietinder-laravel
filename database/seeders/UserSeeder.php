<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// Admin user
      DB::table('users')->insert([
          'name' => 'admin',
          'email' => 'admin@gmail.com',
          'role' => 'admin',
          'password' => Hash::make('admin'),
      ]);

      // Classic user
      DB::table('users')->insert([
          'name' => 'user',
          'email' => 'user@gmail.com',
          'role' => 'guest',
          'password' => Hash::make('user'),
      ]);

    }
}
