<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class RecommendationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      for($i = 0; $i < 5; $i++) {
        DB::table('recommendations')->insert([
            'user_id' => 2,
            'movie_id' => rand(1, 10),
        ]);
      }
    }
}
