# BackOffice variables and code explanations/examples

## Getting database

1. To connect to database, fill out the following info with your connection information in your .env file: 

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=
DB_PASSWORD=

2. Create a laravel databse in PHP my admin

3. To create the tables in your database, use the command `php artisan migrate`. If you wish to undo it, use the command `php artisan migrate:rollback`

## Database explanation

For now, I will only explain the database that are being used.

users: the table of the users. The users have the following columns:
id
name
email
email verified at (for now this is not in use)
password
remmember_token --- this is used to login and out (opens and close a session) ---
the following columns have been done using timestamps()
created_at
updated_at
role --- this is to define the role of the user (admin or not) ---




movies: the table for the list of movies
id
title
synopsis
cover_photo
released_date
movie_length
the following columns have been done using timestamps()
created_at
updated_at




recommendations: the relational table between the users and movies
id
user_id --- this is the foreign key of users ---
movie_id --- this is the foreign key of movies ---
like_dislike --- a boolean to indicate whether or not they like or disliked the recommendation ---




## Admin page

If the user is an admin, only they have access to the admin page where they will be able to use various functionalities. 

the code to the route to the admin page is the following: 

use App\Http\Controllers\AdminController;
Route::get('/admin', [AdminController::class, 'index'])->middleware(['auth', 'role:admin']);

for now as a placeholder, the route is /admin. 
The middleware auth verifies if the user is logged in and the role:admin verifies that the user is an admin. Use them together like the following code if you wish to restrict access to a view. 

## Show all users

The show all user functionality is only available if the user is an admin. it will display the list of all the users. 

    $user = DB::table('users')->get();
    return view('show_users', ['user' => $user]);

The $user variable is what to use in order to call the various informations in the users table. 

Here is an example of how to use it: 

<div class="table-responsive">
<table class="table table-striped table-hover table-condensed">
    <thead>
      <tr>
        <th><strong>id</strong></th>
        <th><strong>name</strong></th>
        <th><strong>email</strong></th>
        <th><strong>role</strong></th>
      </tr>
    </thead>
    <tbody>
@foreach($user as $key => $data)
    <tr>    
      <th>{{$data->id}}</th>
      <th>{{$data->name}}</th>
      <th>{{$data->email}}</th>
      <th>{{$data->role}}</th>          
    </tr>
@endforeach
</tbody>
</table>
</div>

the following code would display the id, name, email, and role of an user. 