<?php

use Illuminate\Support\Facades\Route;

/////////////////////////////////////////////////////////////////////////HOME PAGE AND LOGIN////////////////////////////////////////////////////////////////////////
Auth::routes();
Route::get('/', function () {
    return view('auth.login');
});
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

////////////////////////////////////////////////////////////////////////ADMIN ROUTES////////////////////////////////////////////////////////////////////////


//Main route, show table of users and options
Route::get('/admin', [App\Http\Controllers\UserController::class, 'index'])->middleware(['auth', 'role:admin'])->name('admin');

//Route to input edited user information
Route::get('/edit-user/{id}', [App\Http\Controllers\UserController::class, 'edit'])->middleware(['auth', 'role:admin'])->name('edit-user');

//Route to update the edited user information to the database
Route::put('/update-user/{id}', [App\Http\Controllers\UserController::class, 'update'])->middleware(['auth', 'role:admin'])->name('update-user');

//Route to delete the selected user
Route::delete('/delete-user/{id}', [App\Http\Controllers\UserController::class, 'delete'])->middleware(['auth', 'role:admin'])->name('delete-user');

//Route to block or unblock user
Route::get('/user-status/{user}/{status}',[App\Http\Controllers\UserController::class, 'changeStatus'])->middleware(['auth', 'role:admin'])->name('user-status');

//Route to admin recommendations
Route::get('/admin/recommendations', [App\Http\Controllers\AdminRecommendationController::class, 'list'])->middleware(['auth', 'role:admin'])->name('admin-recommendations');

//Route to search user
Route::get('/admin/search-user', [App\Http\Controllers\UserController::class, 'search'])->middleware(['auth', 'role:admin'])->name('search-user');
