# Movie Tinder - Laravel

## Description

Ce projet contient le front et le backoffice de l'application Movie Tinder

## Installation

@todo: expliquer comment faire l'installation


## Start a Laravel Project and install Bootstrap


follow instructions :


-   npm install

-   composer install

-   npm run dev

-   npm run watch (to compile app.js and app.css files in the Public folder)


Create a .env file and add content from example :


-   cp .env.example .env 

-   php artisan key:generate 

    To add an encrypted key to your .env, if it doesn't add the key in your .env file do it manually and paste the Key (php artisan key:generate --show)


In your index.html link the compiled css and js files !!!

<!--
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <script src="{{asset('js/app.js')}}"></script>
 -->


-   php artisan serv (to launch a local serv)


Let's go !!!



## Développement

@todo: expliquer comment lancer le serveur de dev