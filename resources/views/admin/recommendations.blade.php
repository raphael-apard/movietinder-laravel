@extends('layouts.admin')

@section('title', 'Recommendations')

@section('navbar')
    @parent

    {{--    <p>This is appended to the master navbar.</p> //ici tu peux ajouter qqc au navbar--}}
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4>Admin page: user list</h4>
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Movie</th>
                            <th>User mail</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($recos as $reco)
                            <tr>
                                <td>{{ $reco->id }}</td>
                                <td>{{ $reco->movie->title }}</td>
                                <td>{{ $reco->user->email }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
