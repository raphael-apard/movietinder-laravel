@extends('layouts.admin')

@section('title', 'List Users')

@section('navbar')
    @parent

    {{--    <p>This is appended to the master navbar.</p> //ici tu peux ajouter qqc au navbar--}}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4>Admin page: user list</h4>
                </div>
                <div class="card-body">
                @if($users->isNotEmpty())
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Edit</th>
                                <th>Delete</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->email }}</td>
                                <td>{{ $item->role }}</td>
                                <td>
                                    <a href="{{ route('edit-user', [$item->id]) }}" class="btn btn-primary btn-sm">Edit</a>
                                </td>
                                <td>
                                    <form action="{{ route('delete-user', [$item->id]) }}" method="post">
                                    <input class="btn btn-danger btn-sm" type="submit" value="Delete" />
                                        @method ('delete')
                                        @csrf
                                    </form>
                                </td>
                                <td>
                                        @if ($item->status == 1)
                                            <a href="{{ route('user-status', ['user' => $item->id, 'status' => '0']) }}" class="btn btn-danger btn-sm">   Desactiver le profil </a>
                                        @else
                                            <a href="{{ route('user-status', ['user' => $item->id, 'status' => '1']) }}" class="btn btn-primary btn-sm">   Activer le profil </a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                        <div>
                            <h2>No users found</h2>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
