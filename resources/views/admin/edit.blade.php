@extends('layouts.admin')

@section('title', 'Edit Users')

@section('navbar')
    @parent

{{--    <p>This is appended to the master navbar.</p> //ici tu peux ajouter qqc au navbar--}}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            @if (session('status'))
                <h6 class="alert alert-success">{{ session('status') }}</h6>
            @endif

            <div class="card">
                <div class="card-header">
                    <h4>Edit & Update user
                        <a href="{{ route('admin') }}" class="btn btn-danger float-end">BACK</a>
                    </h4>
                </div>
                <div class="card-body">

                    <form action="{{ route('update-user', [$user->id]) }}" method="POST">
                        @csrf
                        @method('PUT')

                        <div class="form-group mb-3">
                            <label for="">Name</label>
                            <input type="text" name="name" value="{{$user->name}}" class="form-control">
                        </div>
                        <div class="form-group mb-3">
                            <label for="">Email</label>
                            <input type="text" name="email" value="{{$user->email}}" class="form-control">
                        </div>
                        <div class="form-group mb-3">
                            <label for="">Role</label>
                            <input type="text" name="role" value="{{$user->role}}" class="form-control">
                        </div>
                        <div class="form-group mb-3">
                            <button type="submit" class="btn btn-primary">Update user</button>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
